---
layout: page
permalink: /external/markdown/
---

# Markdown
Markdown is a lightweight markup language with plain text formatting syntax which became very popular in past decade and which nowadays serves as a standard in various digital communication channels.

## Main features of Markdown:
  * It has very simple syntax easy to learn.
    
  * It is already widely used in scientific community.
  
  * It separates content from visual presentation so it allows you to focus on writing, not formatting.
  
  * It is a non-proprietary file format.
  
  * It is powerful - allows to format text with very little effort.

  * It is portable - since it is actually plain text, it can be opened by literally all text editors.
  
  * It is machine readable - as simple text, markdown documents can be tracked and version using a versioning system (Git, SVN)
  
  * small file size
  
  * easy to convert to other formats - existing editors and command line tools (e.g. [pandoc](https://pandoc.org/) allows for easy conversion between Markdown and other widely used formats like HTML, PDF, docx, LaTeX, etc.
  

## Syntax

<table>
  <thead>
    <tr>
      <th>Markdown</th>
      <th>Rendered Output</th>
    </tr>
  </thead>
  <tbody>
  <tr>
    <td>
      <code>
        # Header 1<br>
        ## Header 2<br>
        ### Header 3 <br>
        #### Header 4 
      </code>
    </td>
    <td>
      <h1>Header 1</h1><br>
      <h2>Header 2</h2><br>
      <h3>Header 3</h3><br>
      <h4>Header 4</h4>
    </td>
    </tr>
    <tr>
      <td>
        <code>
        To **bold** text, add **two asterisks** or __underscores__ before and after a word or phrase.</br>
        To make text **italic** text, add *one asterisks* or _underscores_.
        </code>
      </td>
      <td>
        To make phrase <strong>bold</strong> text, add <strong>two asterisks</strong> or <strong>underscores</strong> before and after a word or phrase.</br>
        To make text <i>italic</i>, add <i>one asterisks</i> or <i>underscores</i>.
      </td>
    </tr>
    <tr>
      <td>
        <code>
        Use numbers for ordered lists:  <br />
          1. First item<br />
          2. Second item<br />
          3. Third item<br />
          &nbsp;&nbsp;&nbsp;&nbsp;1. Indented item<br />
          &nbsp;&nbsp;&nbsp;&nbsp;2. Indented item<br />
          4. Fourth item
        </code>
      </td>
      <td>
        <ol>
          <li>First item</li>
          <li>Second item</li>
          <li>Third item
            <ol>
              <li>Indented item</li>
              <li>Indented item</li>
            </ol>
          </li>
          <li>Fourth item</li>
        </ol>
      </td>
    </tr>
    <tr>
      <td>
        <code>
        For unordered lists
          * First item<br />
          * Second item<br />
          * Third item<br />
          * Fourth item
        </code>
      </td>
      <td>
        <ul>
          <li>First item</li>
          <li>Second item</li>
          <li>Third item</li>
          <li>Fourth item</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>
        <code>
        Include links referring to a web [page](https://www.markdownguide.org/)
        </code>
      </td>
      <td>
        Include links referring to a web <a href="https://www.markdownguide.org/">page</a>
      </td>
    </tr>
    <tr>
      <td>
        <code>
        Include local pictures: ![My awesome picture](/img/r3_logo.png)
        </code>
      </td>
      <td>
        Include local pictures: <img src="../img/img.png">
      </td>
    </tr>
    <tr>
      <td>
        <code>
        Include code blocks! </br>
        ```</br>
        myAwesomeFunction <- function(x) x+1;</br>
        # Apply function </br>
        myAwesomeFunction(2);</br>
        ```
        </code>
      </td>
      <td>
      Include code blocks! </br>
      <code>
      myAwesomeFunction <- function(x) x+1;</br> 
      # Apply function</br>  
      myAwesomeFunction(2);
      </code>
      </td>
    </tr>
  </tbody>
</table>

This is an overview of basic Markdown features. For more in-build features, please see various online [documentation](https://www.markdownguide.org/basic-syntax/).

### Advanced formatting  
Plain syntax is usually enough. But special requirements lead to development of many of so called ["flavored" markdown syntaxes](https://github.com/commonmark/commonmark-spec/wiki/markdown-flavors) implementing more formatting functionality such as linking different sections, highlighting blocks of code, table extensions, strikethroughs, ...

Another big advantage of Markdown is that it can contain HTML tags, which makes formatting very flexible.


## When to use markdown?
  * **Documentation** - Markdown is a perfect solution for description of a data package, project folder, workflow or code repository. Using Markdown ensures that the description will be accessible to everyone even after decades while still nicely structured. Guide for writing a good README is not covered by this HowTo page but you can find plenty of resources online, e.g.:
    * [guide](https://data.research.cornell.edu/content/readme) from Cornell University, UK
    * [GitHub page](https://github.com/mhucka/readmine) for READMEs in a software repository
    
  * **Blogging and tutorials** - structured document with chunks of code, pictures and results of the analyses can be easily converted to HTML format and posted on personal/team websites.
  
  * **Notes and meeting minutes** - you can use following template for simple and nicely structured meeting notes:
<table>
  <thead>
    <tr>
      <th>Markdown</th>
      <th>Rendered Output</th>
    </tr>
  </thead>
  <tbody>
  <tr>
    <td>
      <code>
    # DocTitle</br>
    ### Attendees:</br>
    ### Location:</br>
    ## Objective / Agenda</br>
    ## Meeting Notes</br>
      * first note </br>
      * second note </br>
      * ...</br>
      </br>
    ## Actions</br>
    - [ ] Josh, Alice and Cindy will do this.</br>
    - [x] Bob stops doing that.</br>
    - [ ] Will and Tom will take care of those
    </code>
    </td>
    <td>
      <code>
    <h1>DocTitle</h1></br>
    <h3> Attendees:<h3></br>
    <h3> Location:</h3></br>
    <h2> Objective / Agenda</h2></br>
    <h2> Meeting Notes</h2></br>
      <ul>
        <li> first note</li>
        <li>second note</li>
        <li>...</li>
      </ul>
    <h2> Actions</h2></br>
    <input type="checkbox" > Josh, Alice and Cindy will do this.</input></br>
    <input type="checkbox" checked="checked"> Bob stops doing that.</input></br>
    <input type="checkbox" > Vanessa and Tom will take care of those</input></br>
    </code>
    </td>
  </tr>
</tbody>
</table>

  * **Analyses,reports and papers** - embedding [R markdown](https://rmarkdown.rstudio.com/) in an R project allows you to include executable code and related text in the very same document. This brings unprecedented level of consistency, shareability and reproducibility into the workflows.
 
  * **Online discussion forums** - many scientific and non-scientific discussion forums and social networks support Markdown for formatting the content of messages (e.g. [StackOverflow](https://stackoverflow.com/editing-help), [GitHub](https://help.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax), Reddit or even Facebook) 
  
  * **Presentations** - variety of tools support Markdown-powered slide generation, e.g. [reveal.js](https://revealjs.com/#/), [HackerSlides](https://github.com/jacksingleton/hacker-slides) or [landslide](https://github.com/adamzap/landslide)
 
## Cases when Markdown is not easily applicable

  * **Official documents** like legal agreements and project proposals could be in theory written in Markdown but it is not yet accepted by administrative processes.
  
  * **Big structured tables** with multilevel headers or special formatting can be written in HTML editors and then pasted into your Markdown document.
  
  * **Thesis writing** requires support of citation managers and additional formatting. This can be achieved but usually is conditioned by embedding Latex into your workflow.

## Editors
Markdown file can be written in any text editor. There are many editors supporting rendering and live preview, here are some of them: 

  * Freeware
    * Atom 
    * Dillinger
    * Visual Studio Code
    * ghostwriter
    * Typora 
    * HackMD
    * ...
    
  * Payware:
    * iA Writer
    * SublimeText
    * ByWord
    
  * Online
    * [GitHub pages](https://pages.github.com/)
    * [stackedit.io](stackedit.io)
